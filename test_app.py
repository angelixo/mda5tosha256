import pytest
from app import *
import os

test_new_passwordsPath = 'test_new_passwords.txt'
test_plainPath = 'test_plain.txt'

# Borra los ficheros generados durante el test
def Cleanup():
    os.unlink(test_new_passwordsPath)
    os.unlink(test_plainPath)

# Ejecuta el script en busca de excepciones
def test_app():
 generateSHA(test_new_passwordsPath,generatePlainFile(test_plainPath, readPlanPasswords()))
 assert os.path.exists(test_new_passwordsPath) & os.path.exists(test_plainPath)
 Cleanup()

# Prueba del generador de SHA
def test_getSHA256():
   sha = 'a6a58719a69791be32db295345519fb71f8e35abb6c1d3ae62185fe8684e321f' # pps0003fckgwrhqq2
   generatedSHA = getSHA256('pps0003fckgwrhqq2')
   assert sha == generatedSHA

# Prueba del transformador de texto
def test_getTransformedText():
    transformedText = 'pps0003fckgwrhqq2'
    generatedTransformedText = getTransformedText('fckgwrhqq2', 2)
    assert transformedText == generatedTransformedText

# Prueba de la API de nitrxgen
def test_getPlainPassword():
   planPassword = 'fckgwrhqq2'
   generatedPlainPassword = getPlainPassword('429839149dcc0fb1564788760015d612') #md5 of fckgwrhqq2
   assert planPassword == generatedPlainPassword
