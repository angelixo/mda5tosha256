import requests
import time
import hashlib
import logging

logging.basicConfig(level=logging.INFO)
passwordsFilePath = './input/passwords.txt'
plainPassworsFilePath = './output/plain.txt'
newPasswordsFilePath = './output/new_passwords.txt'

# Devuelve el hash SHA256 de un value (string:string)
def getSHA256(value):
    sha256 = hashlib.sha256()
    sha256.update(value.encode('utf-8'))
    token = sha256.hexdigest()

    return token

# Devuelve el texto con las modificaciones solicitadas (string, number: string)
def getTransformedText(text, index):
    id = f'{index + 1}'
    transformedPassword = f'pps{id.zfill(4)}{text}'

    return transformedPassword

# Devuelve la contraseña descifrada (string:string)
def getPlainPassword(md5Password):
    url = f'https://www.nitrxgen.net/md5db/{md5Password}.json'
    response = requests.get(url)
    data = response.json()

    return data['result']['pass']

# Lee el fichero origen y decodifica la contraseña
def readPlanPasswords():
    with open(passwordsFilePath, 'r') as passwordsFile:
        plainPasswords = []
        for i,md5PasswordLine in enumerate(passwordsFile):
            md5Password = md5PasswordLine.strip()
            plainPassword = getPlainPassword(md5Password)
            plainPasswords.append(plainPassword)

            logging.info(f"#{i + 1} - MD5: {md5Password} -> Password: {plainPassword}")

            time.sleep(1) # Esperamos para no colapasar el servidor.

    return plainPasswords

# Genera el fichero plain.txt
def generatePlainFile(plainPasswordsFileOutputPath, plainPasswords):
    with open(plainPasswordsFileOutputPath, 'w') as plainPasswordsFile:
        transformedPasswords = []
        for i, plainPassword in enumerate(plainPasswords):
            transformedPassword = getTransformedText(plainPassword, i)
            transformedPasswords.append(transformedPassword)
            plainPasswordsFile.write(transformedPassword + '\n')

            logging.info(f"{i + 1}/299")

        logging.info(f"Se han generado {len(transformedPasswords)} contraseñas transformadas en el fichero plain.txt")

        return transformedPasswords

# Genera el fichero new_passwords
def generateSHA(newPasswordsFileOutputPath,transformedPasswords):
    with open(newPasswordsFileOutputPath, 'w') as newPasswordsFile:
        for transformedPassword in transformedPasswords:
            shaPassword = getSHA256(transformedPassword)
            newPasswordsFile.write(shaPassword + '\n')

        logging.info(f"Se han generado {len(transformedPasswords)} hashes SHA256 en el fichero new_passwords.txt")

# Ejecuta la aplicación
generateSHA(newPasswordsFilePath,generatePlainFile(plainPassworsFilePath, readPlanPasswords()))
